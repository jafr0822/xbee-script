from Utilities import DictionaryGenerator


def read_configuration():

    filepath = 'XbeeConfiguration.txt'
    configuration = DictionaryGenerator.generate(filepath)
    return configuration

import sys

from digi.xbee.io import IOLine

def invalid():
    print("Error")


def parse_arguments(switcher, argument):

    # Get the function from switcher dictionary
    func = switcher.get(argument, invalid)
    print("Function: " + func.__name__)

    if(func != invalid):

        if(len(sys.argv)>2):

            last_argument = str(sys.argv[-1])
            print("Last argument: " + last_argument)

            if(last_argument.startswith('D')):
                io_number = last_argument[1:]
                if(io_number.isdecimal()):
                    io_line = IOLine.get(int(io_number))

                for arg in sys.argv[2:-1]:
                    func(arg, io_line)

            else:
                print(str(sys.argv[2:]))
                for arg in sys.argv[2:]:

                    func(arg)

            return

    return func()


def new_run(switcher):

    print('Number of arguments:', len(sys.argv)-1, 'arguments.')
    print('Argument List:', str(sys.argv[1:]))

    if (len(sys.argv) > 1):
        parse_arguments(switcher, sys.argv[1])
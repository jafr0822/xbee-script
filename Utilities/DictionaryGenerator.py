import os

def generate(filepath):

    dir = os.path.dirname(__file__)
    filename = os.path.join(dir, os.path.pardir, 'Configuration', filepath)
    filename = os.path.normpath(filename)

    with open(filename, 'r') as document:
        dictionary = {}
        for line in document:
            if (line.startswith("#")):
                continue
            line = line.split()
            if not line:  # empty line?
                continue
            dictionary[line[0]] = line[1:]

    return dictionary

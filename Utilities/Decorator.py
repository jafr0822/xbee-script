import warnings
import functools


def _warning(func, reason):
    @functools.wraps(func)
    def new_func(*args, **kwargs):
        warnings.simplefilter('always', UserWarning)  # turn off filter
        warnings.warn("Call to {} function {}.".format(reason, func.__name__),
                      category=UserWarning,
                      stacklevel=2)
        warnings.simplefilter('default', UserWarning)  # reset filter

        return func(*args, **kwargs)
    return new_func

def not_implemented(func, reason='Not implemented yet'):

    return _warning(func, reason)


def not_finished(func, reason='Not finished yet'):

    return _warning(func, reason)

from digi.xbee.devices import XBeeDevice, XBeeNetwork
from digi.xbee.models import address
from Utilities import ConfigurationLoader
import sys


def read_configuration():
    configuration = ConfigurationLoader.read_configuration()

    return configuration


def start_xbee():
    configuration = read_configuration()
    port = configuration.get('Port')[0]
    baud_rate = int(configuration.get('BaudRate')[0])
    local_device = XBeeDevice(port, baud_rate)
    print(str(local_device))

    return local_device


def end_xbee(local_device):
    if local_device is not None and local_device.is_open():
        local_device.close()

    return True


def get_xbee_network(local_device):

    try:

        local_device.open()
        xbee_network = local_device.get_network()

        print("Numero de Dispositivos en red: " + str(xbee_network.get_number_devices()))

        if(not xbee_network.has_devices()):
            raise Exception("No devices in network")

    except Exception as ex:

        print((type(ex)).__name__ + ": " + str(ex))
        sys.exit(-1)

    finally:

        return xbee_network


def get_xbee(xbee_network, id):

    id_string = str(id)
    device = None

    try:

        if (id_string.startswith("n")):
            node_id = id_string[1:]
            device = xbee_network.discover_device(node_id)
        elif (id_string.startswith("a")):
            address_id = id_string[1:]
            xbee_64_address = address.XBee64BitAddress(bytearray.fromhex(address_id))
            device = xbee_network.get_device_by_64(xbee_64_address)
        elif (id_string == "local"):
            device = xbee_network.get_local_xbee_device()

        if(device == None):
            raise Exception("No device found with id: " + id_string )

    except Exception as ex:
        print((type(ex)).__name__ + ": " + str(ex))
        sys.exit(-1)

    return device



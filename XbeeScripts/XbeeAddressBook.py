from Utilities import DictionaryGenerator


def read_address_book():

    filepath = 'XbeeAddressBook.txt'
    address_book = DictionaryGenerator.generate(filepath)
    return address_book


def get_addresses(xbee_group):

    address_book = read_address_book()
    if(xbee_group == 'all'):

        addresses = address_book.values()

    else:

        addresses = address_book.get(xbee_group)

    if (addresses):
        print("Group " + xbee_group + " - Adresses/Node ID's: " + str(addresses))
        return addresses

    else:
        raise KeyError

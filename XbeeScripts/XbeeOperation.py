from digi.xbee.io import IOMode, IOValue
from digi.xbee.util import utils

import time
import threading


def set_io(device, io_line, io_value):

    stop = False
    th = None

    try:

        def io_detection_callback():

            while not stop:
                device.set_dio_value(io_line, io_value)
                time.sleep(0.2)

        th = threading.Thread(target=io_detection_callback)

        device.set_io_configuration(io_line, IOMode.DIGITAL_OUT_LOW)
        time.sleep(0.2)

        th.start()

    finally:
        stop = True
        if th is not None and th.is_alive():
            th.join()


def get_xbee_parameter(device, parameter='all'):

    try:

        if(parameter == 'all'):
            current_value = utils.hex_to_string(device.get_parameter(parameter))

        else:
            current_value = utils.hex_to_string(device.get_parameter(parameter))

    except:

        print("Error in get_parameter function. Device: " + device + " - Parameter: " + parameter)


    return current_value


def set_xbee_parameter(device, parameter, new_value):

    try:

        device.open()
        device.set_parameter(parameter, new_value)
        current_value = utils.hex_to_string(device.get_parameter(parameter))

    except:

        print("Error in get_parameter function. Device: " + device + " - Parameter: " + parameter)

    if(current_value == new_value):
        return True
    else:
        return False

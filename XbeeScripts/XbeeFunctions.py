from XbeeScripts import XbeeAddressBook, XbeeOperation, XbeeConnection
from digi.xbee.io import IOLine, IOValue
import traceback
from Utilities import Decorator

def on(xbee_group='all', io_line=IOLine.DIO0_AD0):

    try:
        xbee_ids = XbeeAddressBook.get_addresses(xbee_group)
        local_device = XbeeConnection.start_xbee()
        xbee_network = XbeeConnection.get_xbee_network(local_device)

        for xbee_id in xbee_ids:
            device = XbeeConnection.get_xbee(xbee_network, xbee_id)
            print("Device: " + str(device))
            XbeeOperation.set_io(device, io_line, IOValue.HIGH)

    except(KeyError):
        print("XBee " + xbee_group + " not in address book")

    except Exception as ex:
        print((type(ex)).__name__ + ": " + str(ex))
        traceback.print_exc()

    finally:
        if(local_device):
            if(local_device.is_open()):
                local_device.close()


@Decorator.not_finished
def off(xbee_group='all', io_line=IOLine.DIO0_AD0):
    try:
        xbee_ids = XbeeAddressBook.get_addresses(xbee_group)
        local_device = XbeeConnection.start_xbee()
        xbee_network = XbeeConnection.get_xbee_network(local_device)

        for xbee_id in xbee_ids:
            device = XbeeConnection.get_xbee(xbee_network, xbee_id)
            print("Device: " + str(device))
            XbeeOperation.set_io(device, io_line, IOValue.LOW)

    except(KeyError):
        print("XBee " + xbee_group + " not in address book")

    except Exception as ex:
        print((type(ex)).__name__ + ": " + str(ex))
        traceback.print_exc()

    finally:
        if(local_device):
            if(local_device.is_open()):
                local_device.close()


@Decorator.not_finished
def get_io_state(xbee_group='all', io_line=IOLine.DIO0_AD0):
    print("IO state " + xbee_group)


@Decorator.not_implemented
def get_parameter(xbee_group='all', parameter='DL'):

    print("Parameter " + parameter + " " + xbee_group)
    try:
        xbee_address = XbeeAddressBook.get_address(xbee_group)
        print(xbee_group +" "+str(xbee_address))
        device = XbeeConnection.start_xbee()

    except(KeyError):
        print("Xbee " + xbee_group + " not in address book")


function_switcher = {
        'on': on,
        'off': off,
        'state': get_io_state,
    }